package acme;

import java.lang.String;
import java.lang.System;

public class ThirtyThree {
    public String fullySpelledOut() {
        return "👋 thirty three 😃";
    }

    public void printMe() {
        System.out.println(this.fullySpelledOut());
    }

    public static void main( String[] args )
    {
        ThirtyThree myNumber = new ThirtyThree();
        System.out.println(myNumber.fullySpelledOut());
    }

}
